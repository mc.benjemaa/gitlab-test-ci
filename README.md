# Sample 3tier app

This repo contains code for a Node.js multi-tier application.

The application overview is as follows

```
web <=> api <=> db
```

The folders `web` and `api` respectively describe how to install and run each app.

### Deployment

Deployment is made to Kubernetes cluster.

Each app has a helm chart in `helm/` directory.
