## nginx-ingress

### DNS

`node-app.justk8s.com`


#### Install

```
helm install nginx-ingress ingress-nginx/ingress-nginx -n nginx-ingress
```

Modify A record in the DNS record `node-app`