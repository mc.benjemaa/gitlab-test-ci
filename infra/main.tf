terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "4.53.1"
    }
  }
  backend "http" {}
}

provider "google" {
  project = "playground-378316"
  region  = "us-central1"
}