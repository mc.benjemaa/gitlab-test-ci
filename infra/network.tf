resource "google_compute_network" "peering_network" {
  name                    = "private-network"
  auto_create_subnetworks = "false"
}

# [START vpc_postgres_instance_private_ip_address]
resource "google_compute_global_address" "private_ip_address" {
  name          = "private-ip-address"
  purpose       = "VPC_PEERING"
  address_type  = "INTERNAL"
  prefix_length = 16
  network       = google_compute_network.peering_network.id
}
# [END vpc_postgres_instance_private_ip_address]

# [START cloud_sql_sqlserver_instance_private_ip_routes]
resource "google_compute_network_peering_routes_config" "peering_routes" {
  peering              = google_service_networking_connection.default.peering
  network              = google_compute_network.peering_network.name
  import_custom_routes = true
  export_custom_routes = true
}
# [END cloud_sql_sqlserver_instance_private_ip_routes]


resource "google_compute_subnetwork" "network-with-private-secondary-ip-ranges" {
  name          = "app-subnetwork"
  ip_cidr_range = "10.2.0.0/16"
  region        = "us-central1"
  network       = google_compute_network.peering_network.id

  secondary_ip_range = [
    {
      range_name    = "app-secondary-range"
      ip_cidr_range = "192.168.10.0/24"
    },
    {
      range_name    = "pods-ip-ranges"
      ip_cidr_range = "10.234.0.0/17"
    },
    {
      ip_cidr_range = "10.234.128.0/22"
      range_name    = "services-ip-ranges"
    }
  ]
}