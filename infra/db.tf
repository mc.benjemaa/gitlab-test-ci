
# [START vpc_postgres_instance_private_ip_service_connection]
resource "google_service_networking_connection" "default" {
  network                 = google_compute_network.peering_network.id
  service                 = "servicenetworking.googleapis.com"
  reserved_peering_ranges = [google_compute_global_address.private_ip_address.name]
}
# [END vpc_postgres_instance_private_ip_service_connection]


# [START cloud_sql_postgres_instance_private_ip_instance]
resource "google_sql_database_instance" "default" {
  name             = var.db_name
  region           = "us-central1"
  database_version = "POSTGRES_14"

  depends_on = [google_service_networking_connection.default]

  settings {
    tier = "db-custom-2-7680"
    ip_configuration {
      ipv4_enabled    = "false"
      private_network = google_compute_network.peering_network.id
    }
    backup_configuration {
      enabled    = true
      start_time = "12:00"
    }
  }
  deletion_protection = false
}

resource "google_sql_database" "database" {
  name     = "app"
  instance = google_sql_database_instance.default.name
}

resource "google_sql_user" "users" {
  name     = var.db_username
  instance = google_sql_database_instance.default.name
  password = var.db_password
}

data "google_project" "playgroung" {
}

resource "google_service_account" "db_proxy" {
  account_id   = "db-proxy"
  display_name = "db-proxy service Account"
}

resource "google_project_iam_binding" "cloudsql-sa-cloudsql-admin-role" {
  project = data.google_project.playgroung.id
  role    = "roles/cloudsql.admin"
  members = [
    "serviceAccount:${google_service_account.db_proxy.email}",
    "serviceAccount:infra-34@playground-378316.iam.gserviceaccount.com"
  ]
}

resource "google_service_account_key" "cloudsql_key" {
  service_account_id = google_service_account.db_proxy.name
}

