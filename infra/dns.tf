resource "google_dns_managed_zone" "node-zone" {
  name        = "node-app"
  dns_name    = "node-app.${var.domain}."
  description = "Node App DNS zone"
}
