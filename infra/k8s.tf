resource "google_service_account" "default" {
  account_id   = "service-account-id"
  display_name = "gke service Account"
}

resource "google_container_cluster" "primary" {
  name     = var.cluster_name
  location = "us-central1-f"

  network                  = google_compute_network.peering_network.id
  subnetwork               = google_compute_subnetwork.network-with-private-secondary-ip-ranges.id
  remove_default_node_pool = true
  initial_node_count       = 1
  cluster_autoscaling {
    enabled = true
    resource_limits {
      resource_type = "cpu"
      minimum       = 2
      maximum       = 20
    }
    resource_limits {
      resource_type = "memory"
      minimum       = 4
      maximum       = 32
    }
  }
  ip_allocation_policy {
    cluster_secondary_range_name  = "pods-ip-ranges"
    services_secondary_range_name = "services-ip-ranges"
  }

}


resource "google_container_node_pool" "primary_preemptible_nodes" {
  name     = "my-node-pool"
  location = "us-central1-f"

  cluster    = google_container_cluster.primary.name
  node_count = 2

  node_config {
    preemptible  = true
    machine_type = "e2-standard-2"
    disk_size_gb = 50

    # Google recommends custom service accounts that have cloud-platform scope and permissions granted via IAM Roles.
    service_account = google_service_account.default.email
    oauth_scopes = [
      "https://www.googleapis.com/auth/cloud-platform"
    ]
  }
}

data "google_client_config" "default" {
  depends_on = [google_container_cluster.primary]
}

provider "kubernetes" {
  host  = "https://${google_container_cluster.primary.endpoint}"
  token = data.google_client_config.default.access_token
  cluster_ca_certificate = base64decode(
    google_container_cluster.primary.master_auth[0].cluster_ca_certificate,
  )
}
