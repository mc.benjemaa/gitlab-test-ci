variable "domain" {
  description = "Base Domain"
  type        = string
}
variable "remote_state_address" {
  type        = string
  description = "Gitlab remote state file address"
}

variable "remote_state_username" {
  type        = string
  description = "Gitlab username to query remote state"
}

variable "remote_state_access_token" {
  type        = string
  description = "GitLab access token to query remote state"
}

variable "db_name" {
  type = string
}

variable "cluster_name" {
  type = string
}

variable "db_username" {
  description = "Database administrator username"
  type        = string
  sensitive   = true
}

variable "db_password" {
  description = "Database administrator password"
  type        = string
  sensitive   = true
}