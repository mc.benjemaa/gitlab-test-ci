## Infra

### Login with GCP

```
gcloud auth application-default login
```


### Terraform remote state Gitlab 

```
export PROJECT_ID="30279"
export TF_USERNAME="mc.benjemaa"
export TF_PASSWORD="<Personel Access Token>"
export TF_ADDRESS="https://git.toptal.com/api/v4/projects/30279/terraform/state/tf_state"
```


```
terraform init \
  -backend-config=address=${TF_ADDRESS} \
  -backend-config=lock_address=${TF_ADDRESS}/lock \
  -backend-config=unlock_address=${TF_ADDRESS}/lock \
  -backend-config=username=${TF_USERNAME} \
  -backend-config=password=${TF_PASSWORD} \
  -backend-config=lock_method=POST \
  -backend-config=unlock_method=DELETE \
  -backend-config=retry_wait_min=5
```