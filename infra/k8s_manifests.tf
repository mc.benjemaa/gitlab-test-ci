resource "kubernetes_namespace" "app" {
  metadata {
    name = "app"
  }
}

resource "kubernetes_namespace" "nginx" {
  metadata {
    name = "nginx-ingress"
  }
}

# Secret for SA required for DB proxy  
resource "kubernetes_secret" "google-application-credentials" {
  metadata {
    name = "db-sa-proxy"
    namespace = "app"
  }
  data = {
    "service_account.json" = base64decode(google_service_account_key.cloudsql_key.private_key)
  }
}