apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "api.fullname" . }}
  labels:
    {{- include "api.labels" . | nindent 4 }}
spec:
  {{- if not .Values.autoscaling.enabled }}
  replicas: {{ .Values.replicaCount }}
  {{- end }}
  selector:
    matchLabels:
      {{- include "api.selectorLabels" . | nindent 6 }}
  template:
    metadata:
      {{- with .Values.podAnnotations }}
      annotations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      labels:
        {{- include "api.selectorLabels" . | nindent 8 }}
    spec:
      volumes:
      - name: db-proxy
        secret:
          secretName: db-sa-proxy
      {{- with .Values.imagePullSecrets }}
      imagePullSecrets:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      serviceAccountName: {{ include "api.serviceAccountName" . }}
      securityContext:
        {{- toYaml .Values.podSecurityContext | nindent 8 }}
      containers:
        - name: cloud-sql-proxy
          image: gcr.io/cloud-sql-connectors/cloud-sql-proxy:2.0.0  # make sure to use the latest version
          args:
            - "--structured-logs"
            - "--private-ip"
            - "--port=5432"
            - {{ .Values.instanceConnectionName }}
            - "--credentials-file=/secrets/service_account.json"
          securityContext:
            runAsNonRoot: true
          resources:
            requests:
              memory: "2Gi"
              cpu:    "1"
          volumeMounts:
          - name: db-proxy
            mountPath: /secrets/
            readOnly: true
        - name: {{ .Chart.Name }}
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: "{{ .Values.image.repository }}:{{ .Values.image.tag | default .Chart.AppVersion }}"
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          ports:
            - name: http
              containerPort: 8080
              protocol: TCP
          env:
          - name: PORT
            value: "8080"
          - name: DB
            valueFrom:
              secretKeyRef:
                name: {{ .Values.dbSecret.name }}
                key: {{ .Values.dbSecret.dbKey }}
          - name: DBUSER
            valueFrom:
              secretKeyRef:
                name: {{ .Values.dbSecret.name }}
                key: {{ .Values.dbSecret.dbUser }}
          - name: DBPASS
            valueFrom:
              secretKeyRef:
                name: {{ .Values.dbSecret.name }}
                key: {{ .Values.dbSecret.dbPass }}
          - name: DBHOST
            valueFrom:
              secretKeyRef:
                name: {{ .Values.dbSecret.name }}
                key: {{ .Values.dbSecret.dbHost }}
          - name: DBPORT
            valueFrom:
              secretKeyRef:
                name: {{ .Values.dbSecret.name }}
                key: {{ .Values.dbSecret.dbPort }} 
          resources:
            {{- toYaml .Values.resources | nindent 12 }}
      {{- with .Values.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.affinity }}
      affinity:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
